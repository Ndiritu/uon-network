#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

EXAM_CC_NAME=ccExam
EXAM_APPROVAL_CC_NAME=ccExamApproval
QSTN_CC_NAME=ccQstn 
INSTRUCTION_CC_NAME=ccInstruction
SECTION_CC_NAME=ccSection
COMMENT_CC_NAME=ccComment
ITEM_MOD_CC_NAME=ccItemMod
EXAM_GENERATION_CC_NAME=ccExamGeneration

EXAM_CC_VER=7.0
EXAM_APPROVAL_CC_VER=1.9
QSTN_CC_VER=3.4
INSTRUCTION_CC_VER=3.4
SECTION_CC_VER=2.4
COMMENT_CC_VER=1.7
ITEM_MOD_CC_VER=1.7
EXAM_GENERATION_CC_VER=1.0

# Upgrade Chaincode
# peer chaincode upgrade -o orderer.example.com:7050 -C channel-exams -n ccInstruction -v 3.4 -c '{"Args":[]}' 

sudo rm -rf /var/hyperledger/data/peer0/*
sudo rm -rf /var/hyperledger/data/peer1/*
sudo rm -rf /var/hyperledger/data/orderer/*

docker-compose -f docker-compose.yml down

docker-compose -f docker-compose.yml up -d 

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=10
#echo ${FABRIC_START_TIMEOUT}
sleep ${FABRIC_START_TIMEOUT}

# Create channels

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel create -o orderer.example.com:7050 -c channel-exams -f /etc/hyperledger/configtx/channelExams.tx
echo "CREATED CHANNEL CONFIG FOR CHANNEL-EXAMS"
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel create -o orderer.example.com:7050 -c channel-results -f /etc/hyperledger/configtx/channelResults.tx
echo "CREATED CHANNEL-CONFIG FOR CHANNEL-RESULTS"

# # Join peer0.org1.example.com to the channel.
sleep 5
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel join -b channel-exams.block
echo "peer0.org1.example.com JOINED CHANNEL-EXAMS"
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel join -b channel-results.block
echo "peer0.org1.example.com JOINED CHANNEL-RESULTS"


# TODO: Join peer1 to the channels!
# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" -e "peer1.org1.example.com:7053" peer0.org1.example.com peer channel join -b channel-exams.block
# echo "peer1.org1.example.com JOINED CHANNEL-EXAMS"
# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" -e "peer1.org1.example.com:7053" peer0.org1.example.com peer channel join -b channel-results.block
# echo "peer1.org1.example.com JOINED CHANNEL-RESULTS"

# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer1.org1.example.com peer channel join -b channel-exams.block
# echo "peer1.org1.example.com JOINED CHANNEL-EXAMS"
# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer1.org1.example.com peer channel join -b channel-results.block
# echo "peer1.org1.example.com JOINED CHANNEL-RESULTS"


# Update channel definition by adding anchor peers
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel update -o orderer.example.com:7050 -c channel-exams -f /etc/hyperledger/configtx/Org1MSPExamsAnchors.tx
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel update -o orderer.example.com:7050 -c channel-results -f /etc/hyperledger/configtx/Org1MSPResultsAnchors.tx


# Install chaincode on peer0
docker exec cli peer chaincode install -n $EXAM_CC_NAME -v $EXAM_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/examination
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $EXAM_CC_NAME -v $EXAM_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $EXAM_APPROVAL_CC_NAME -v $EXAM_APPROVAL_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/examination-approval
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $EXAM_APPROVAL_CC_NAME -v $EXAM_APPROVAL_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $QSTN_CC_NAME -v $QSTN_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/question
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $QSTN_CC_NAME -v $QSTN_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $INSTRUCTION_CC_NAME -v $INSTRUCTION_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/instruction 
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $INSTRUCTION_CC_NAME -v $INSTRUCTION_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $SECTION_CC_NAME -v $SECTION_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/section 
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $SECTION_CC_NAME -v $SECTION_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $COMMENT_CC_NAME -v $COMMENT_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/comment
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $COMMENT_CC_NAME -v $COMMENT_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $ITEM_MOD_CC_NAME -v $ITEM_MOD_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/item-moderation
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $ITEM_MOD_CC_NAME -v $ITEM_MOD_CC_VER -c '{"Args": []}'

docker exec cli peer chaincode install -n $EXAM_GENERATION_CC_NAME -v $EXAM_GENERATION_CC_VER -p github.com/hyperledger/fabric/peer/chaincode/examination-generation
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -C channel-exams -n $EXAM_GENERATION_CC_NAME -v $EXAM_GENERATION_CC_VER -c '{"Args": []}'

# sleep 10

# Examination chaincode tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccExam -c '{"function": "createExamination", "Args": ["1", "January 2018", "ORDINARY", "1", "1",  "1", "12412412", "124125", "7200000", "NUMERICAL", "ALPHABETICAL"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccExam -c '{"Args":["getAllExaminations"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExam -c '{"Args":["getExaminationsByCourseId","1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExam -c '{"Args":["getExaminationsByLecturerId","1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExam -c '{"Args":["getExaminationById", "1"]}'
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccExam -c '{"function": "updateExamination", "Args":["1", "1", "Feb 2018", "ORDINARY", "1", "4387503485", "63426436", "7200000", "NUMERICAL", "ALPHABETICAL"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccExam -c '{"Args": ["getExamHistoryById", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExam -c '{"Args": ["deleteExamination", "1", "1"]}'


# Exam approval tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccExamApproval -c '{"function": "createExamApproval", "Args":["1", "1", "true"]}'
# sleep 5
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccExamApproval -c '{"function": "updateExamApproval", "Args":["1", "1", "false"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccExamApproval -c '{"Args": ["getExamsByApprovalStatus", "1", "false"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExamApproval -c '{"Args": ["getApprovalByExamId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExamApproval -c '{"Args": ["getApprovalHistoryByExamId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExamApproval -c '{"Args": ["getAllExamApprovals"]}'


# Exam generation tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccExamGeneration -c '{"function": "createExamGeneration", "Args":["1", "1", "true", "35", "1", "false", "0"]}'
# sleep 5
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccExamGeneration -c '{"function": "updateExamGeneration", "Args":["1", "EXAM_CENTER_STAFF", "1", "false", "0"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccExamGeneration -c '{"Args": ["getGenerationInfoByExamId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExamGeneration -c '{"Args": ["getGenerationInfoByExamCentreStaffId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccExamGeneration -c '{"Args": ["getAllExamGenerations"]}'

# Question chaincode tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccQstn -c '{"function": "createQuestion", "Args":["1", "1", "0", "MAIN_QUESTION", "What is your name?", "5"]}'
# sleep 5
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccQstn -c '{"function": "updateQuestion", "Args":["1", "1", "1", "SUB_QUESTION", "What is your FIRST NAME?", "2"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccQstn -c '{"Args": ["getQuestionById", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccQstn -c '{"Args": ["getQuestionsByExamId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccQstn -c '{"Args": ["getAllQuestions"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccQstn -c '{"Args": ["getQuestionHistory", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccQstn -c '{"Args": ["deleteQuestion", "1", "1"]}'


# Instruction chaincode test
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccInstruction -c '{"function": "createInstruction", "Args":["1", "1", "1", "EXAM_LEVEL", "Kindly write your name"]}'
# sleep 5
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccInstruction -c '{"function": "updateInstruction", "Args":["1", "1", "1", "SECTION_LEVEL", "What is your LAST NAME?"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccInstruction -c '{"Args": ["getInstructionsByExamId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccInstruction -c '{"Args": ["getInstructionById", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccInstruction -c '{"Args": ["getInstructionHistoryById", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccInstruction -c '{"Args": ["deleteInstruction", "1", "1"]}'


# Section chaincode tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccSection -c '{"function": "createSection", "Args":["1", "1", "2", "A"]}'
# sleep 5
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccSection -c '{"function": "updateSection", "Args":["1", "1", "1", "B"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccSection -c '{"Args": ["getSectionsByExamId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccSection -c '{"Args": ["getSectionById", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccSection -c '{"Args": ["getAllSections"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccSection -c '{"Args": ["deleteSection", "1", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccSection -c '{"Args": ["getSectionHistory", "1"]}'


# Comment chaincode tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccComment -c '{"function": "createComment", "Args":["1", "Please check spelling error", "EXT_EXAMINER", "1", "LECTURER", "1", "QUESTION", "1", "1"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getCommentsByItem", "QUESTION", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getCommentsByExam", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getCommentsBySender", "EXT_EXAMINER", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getCommentsByRecipient", "LECTURER", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["deleteComment", "1", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getAllComments"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getCommentHistoryById", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccComment -c '{"Args": ["getCommentById", "1"]}'


# Item moderation tests
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccItemMod -c '{"function": "createItemModeration", "Args":["QUESTION", "1", "1", "true"]}'
# sleep 5
# docker exec cli peer chaincode invoke -o orderer.example.com:7050 -C channel-exams -n ccItemMod -c '{"function": "updateItemModeration", "Args":["1", "1", "false"]}'
# sleep 5
# docker exec cli peer chaincode query -C channel-exams -n ccItemMod -c '{"Args": ["getModerationByItem", "QUESTION", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccItemMod -c '{"Args": ["getModerationsByExtExaminerId", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccItemMod -c '{"Args": ["getModerationHistoryByItem", "1"]}'
# docker exec cli peer chaincode query -C channel-exams -n ccItemMod -c '{"Args": ["getAllModerations"]}'

# QSCC test
# docker exec cli peer chaincode query -C channel-exams -n qscc -c '{"Args": ["GetChainInfo", "channel-exams"]}'
# docker exec cli peer chaincode query -C channel-exams -n qscc -c '{"Args": ["GetTransactionByID", "channel-exams", "ace6000e999bbe41851759d5e39589ae837755c619a17fa56df919950a2b5a01"]}'